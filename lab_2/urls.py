from django.urls import path
import lab_2.views as views

urlpatterns = [
    path('', views.index, name='index'),
    path('xml', views.xml, name='index'),
    path('json', views.json, name='index'),
]

# /lab-2/xml