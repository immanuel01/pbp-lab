from django.db import models

# Create your models here.


class Note(models.Model):
    dari = models.CharField(max_length=30)
    to = models.CharField(max_length=30)
    title = models.CharField(max_length=45)
    message = models.CharField(max_length=240)

