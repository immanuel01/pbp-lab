from django.shortcuts import render
from django.http.response import HttpResponse
from django.core import serializers
from lab_2.models import *


# Create your views here.
def index(request):
    response = {'notes': Note.objects.all()}
    return render(request, "lab2.html", response)


def xml(request):
    note_objects = Note.objects.all()
    data = serializers.serialize('xml', note_objects)
    return HttpResponse(data, content_type="application/xml")


def json(request):
    note_objects = Note.objects.all()
    data = serializers.serialize('json', note_objects)
    return HttpResponse(data, content_type="application/json")
