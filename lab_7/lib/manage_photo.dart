// ignore_for_file: unused_local_variable

import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import "package:lab_7/halaman_toko.dart";


void main() {
  runApp(ManagePhotoMaterial());
}



class ManagePhotoMaterial extends StatelessWidget{
  const ManagePhotoMaterial({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
            fontSizeFactor: 1.3,
            fontSizeDelta: 2.0,
            fontFamily: 'Tisan'
        ),
      ),

      home: ManagePhotoBody("Bizzvest", "PT. Bizzvest Indonesia"),
    );
  }
}



class ManagePhotoBody extends StatefulWidget{
  final String nama_merek;
  final String nama_perusahaan;
  ManagePhotoBody(this.nama_merek, this.nama_perusahaan, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _ManagePhotoBody(nama_merek, nama_perusahaan);
  }


}

class _ManagePhotoBody extends State<ManagePhotoBody>{
  final String nama_merek;
  final String nama_perusahaan;
  final GlobalKey<ScaffoldState> _scaffold_key = GlobalKey<ScaffoldState>();
  _ManagePhotoBody(this.nama_merek, this.nama_perusahaan);

  final ImagePicker _picker = ImagePicker();

  List<Widget> photo_items = [];


  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback(
      (_) {
          if (_scaffold_key == null || _scaffold_key.currentContext == null)
            return;
          ScaffoldMessenger.of(_scaffold_key.currentContext!).showSnackBar(
              const SnackBar(content: Text(
                  "Please long press and hold to reorder the images \n"
                  + "Double tap to delete photos"
              ))
          );
      }
    );
  }

  void on_delete(int index){
    setState(() {
      photo_items.removeAt(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    TextFormField berakhir_date;

    return
        Scaffold(
          key: _scaffold_key,
          floatingActionButton: FloatingActionButton(
            child: FaIcon(FontAwesomeIcons.plus),
            onPressed: () async {
              final List<XFile>? images = await _picker.pickMultiImage();
              setState(() {
                images?.forEach((element) {
                  int this_index = photo_items.length;

                  photo_items.add(
                    ImageTile(
                      Image.file(
                          File(element.path)
                      ),
                      key: UniqueKey(),
                      on_double_tap: (){
                        on_delete(this_index);
                      },
                    )
                  );
                });
              });
            },
          ),
          body:
            SafeArea(
              child: Container(
                margin: EdgeInsets.symmetric(vertical: 20),
                child: BorderedContainer(
                  Container(
                    margin: EdgeInsets.all(20),
                    child: ReorderableListView(
                      children: photo_items,

                      onReorder: (oldIndex, newIndex) {
                        setState(() {
                          if (oldIndex < newIndex) {
                            newIndex -= 1;
                          }
                          photo_items.insert(newIndex, photo_items.removeAt(oldIndex));
                        });
                      },
                    ),
                  ),
                ),
              ),
            ),
          backgroundColor: (Colors.lightBlue[200])!,

        );
  }
}



class ImageTile extends StatelessWidget{
  Image img;
  ImageTile(this.img, {Key? key, this.on_double_tap}) : super(key: key);
  Function()? on_double_tap;


  @override
  Widget build(BuildContext context) {
    const EdgeInsets padding_blur = EdgeInsets.symmetric(horizontal: 10);
    const EdgeInsets padding_non_blur = EdgeInsets.symmetric(
      horizontal: 10,
      vertical: 3,
    );

    return Container(
      margin: EdgeInsets.all(5),
      child: GestureDetector(
        onDoubleTap: on_double_tap,

        child: ClipRect(
          child: Center(
            child: SizedBox(
              height: 220,
              width: 220,
              child: Stack(
                children: [
                  Positioned.fill(
                      child: FilteredImage(
                        img: img.image,
                        padding: padding_blur,
                      )
                  ),
                  Positioned.fill(child: Padding(
                    padding: padding_non_blur,
                    child: img,
                  )),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class FilteredImage extends StatelessWidget{
  ImageProvider<Object> img;
  ImageFilter image_filter = ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0);
  EdgeInsets padding;
  BoxFit fit;

  FilteredImage({
                 required this.img,
                 this.padding: const EdgeInsets.all(10),
                 ImageFilter? image_filter: null,
                 this.fit: BoxFit.cover,
               }){
    if (image_filter != null)
      this.image_filter = image_filter;
  }

  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding: padding,
      child: Container(
          child: BackdropFilter(
            filter: image_filter,
            child: Container(
              decoration: BoxDecoration(color: Colors.white.withOpacity(0.0)),
            ),
          ),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: img,
              fit: fit,
            ),
          )
      ),
    );
  }
}