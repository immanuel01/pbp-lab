// noinspection EqualityComparisonWithCoercionJS,JSJQueryEfficiency

$(document).ready(function() {
    TableOfFriends.this_tbody_element = $("#table_of_friends tbody");
    TableOfFriends.refresh();

    // automatically refresh data every 10 seconds
    setInterval(function(){ TableOfFriends.refresh();}, 10000);


    var myModal = document.getElementById('myModal');
    var myInput = document.getElementById('myInput');

    myModal.addEventListener('shown.bs.modal', function () {
        myInput.focus();
    })
});

function get_template(template_element){
    return document.importNode(template_element.prop('content'), true);
}

function add_new_row(table_tbody_element, table_datas) {
    var new_row = document.createElement("tr");

    for (let i = 0; i < table_datas.length; i++) {
        if (is_string(table_datas[i])) {
            var new_td = document.createElement("td");
            new_td.innerHTML = table_datas[i];
            new_row.appendChild(new_td);
        }else if (table_datas[i] instanceof DocumentFragment){
            new_row.appendChild(table_datas[i]);
        }else{
            console.log(table_datas[i]);
            console.log(table_datas[i] instanceof String);
        }
    }

    table_tbody_element.append(new_row);
}

function is_string(obj){
    return typeof(obj) == 'string' || obj instanceof String;
}

function clear_table(table_tbody_element){
    table_tbody_element.find("tr").remove();
}

class TableOfFriends{
    static this_tbody_element;

    static add_row(from, to, title, message){
        add_new_row(TableOfFriends.this_tbody_element, [
            from, to, title, message, get_template($("#action_controls_template"))
        ]);
    }

    static clear_table(){
        clear_table(TableOfFriends.this_tbody_element);
    }

    static load_json(parsed_json){
        console.assert(Array.isArray(parsed_json));
        for (let i = 0; i < parsed_json.length; i++) {
            var item = parsed_json[i];
            if ('model' in item  &&  item['model'] == "lab_2.note"  &&  "fields" in item){
                var fields = item['fields'];

                TableOfFriends.add_row(
                    fields['dari'],
                    fields['to'],
                    fields['title'],
                    fields['message']
                );
            }
        }
    }

    static refresh(){
        $.get(
            "/lab-2/json", function (data, status) {
                if (status == 'success') {
                    TableOfFriends.clear_table();
                    TableOfFriends.load_json(data);
                    console.log("refreshed successfully");
                }
            }
        );
    }

}

