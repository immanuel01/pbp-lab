from django.shortcuts import render
from lab_2.models import Note

def index(req):
    response = {'notes': Note.objects.all()}
    return render(req, "lab5_index.html", response)