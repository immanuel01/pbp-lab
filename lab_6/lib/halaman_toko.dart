import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';


void main() {
  runApp(HalamanTokoMaterial());
}

class HalamanTokoMaterial extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
          fontSizeFactor: 1.3,
          fontSizeDelta: 2.0,
          fontFamily: 'Tisan'
        ),
      ),

      home: SafeArea(child: Scaffold(
        /*appBar: AppBar(
        title: Text("Halaman Toko"),
        centerTitle: true,
      ),*/

        body:SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 10),
          child: HalamanTokoBody("Bizzvest", "PT. Bizzvest Indonesia"),
        ),
        backgroundColor: (Colors.lightBlue[200])!,

        floatingActionButton: null,
      ),
      ),
    );
  }
}


class HalamanTokoBody extends StatelessWidget{
  final String nama_merek;
  final String nama_perusahaan;

  HalamanTokoBody(this.nama_merek, this.nama_perusahaan);

  @override
  Widget build(BuildContext context) {
    return
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [

          HalamanTokoHeaderTitle(nama_merek, nama_perusahaan),

          BorderedContainer(
            AspectRatio(
              aspectRatio: 1 / 1,
              child: ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(7)),
                child: Image.asset("src/img/img1.jpg",
                  fit: BoxFit.cover,

                ),
              )
            ),
          ),
          HalamanTokoOwnerContainer(
              Image.asset("src/img/profile.jpg"),
              "Kugel Blitz",
              "hzz"
          ),
          HalamanTokoKodeSisaPeriode(),
          BorderedButtonIcon(
            onPressed: (){},
            icon: FaIcon(FontAwesomeIcons.book),
            label: Text("Download Proposal"),
          ),
          BorderedContainer(
              HalamanTokoTabularData(
                  lines: const [
                    "Status", "terverifikasi",
                    "Berakhir pada", "01 Jan 2024",
                    "Jumlah saham", "100 lembar",
                    "Harga saham", "Rp1.000.000,00",
                  ])
          ),
          BorderedContainer(
              HalamanTokoKondisiSaham()
          ),


          BorderedContainer(
              HalamanTokoAlamatDeskripsi()
          )
        ],
      );
  }
}

class HalamanTokoHeaderTitle extends StatelessWidget{
  final String nama_merek;
  final String nama_perusahaan;

  HalamanTokoHeaderTitle(this.nama_merek, this.nama_perusahaan);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 12, bottom: 5),
      child: Column(
        children: [
          Container(
            alignment: Alignment.center,
            child: Text(
              this.nama_merek,
              textScaleFactor: 1.8,
              style: const TextStyle(
                fontWeight: FontWeight.bold,
                color: Color.fromARGB(255, 14, 99, 223),
              ),
            ),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              this.nama_perusahaan,
              textScaleFactor: 1.05,
              style: const TextStyle(
                fontWeight: FontWeight.w600,
                color: Color.fromARGB(255, 108, 117, 125),
              ),
            ),
          ),
        ],
      ),
    );
  }
}


class ColouredHeaderText extends StatelessWidget{
  final double textScaleFactor;
  final Color color;
  final FontWeight fontWeight;
  final String fontFamily;
  final String text;

  const ColouredHeaderText(this.text, {
    this.color: const Color.fromARGB(255, 7, 130, 159),
    this.fontWeight: FontWeight.bold,
    this.fontFamily: "Quicksand",
    this.textScaleFactor: 1.0,
  });


  TextStyle get_text_style(){
    return TextStyle(
      color: color,
      fontWeight: fontWeight,
      fontFamily: fontFamily,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: get_text_style(),
      textScaleFactor: textScaleFactor,
    );
  }

}



class HalamanTokoOwnerContainer extends StatelessWidget{
  final Widget foto_profile;
  final String nama_lengkap;
  final String username;

  HalamanTokoOwnerContainer(this.foto_profile, this.nama_lengkap,
      this.username);

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
      Row(
        children: [
          Container(
            width: 100,
            padding: EdgeInsets.all(4),
            margin: EdgeInsets.all(1),
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                // constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
                child: ClipOval(
                  child: foto_profile,
                ),
              ),
            ),
          ),

          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: Text(
                      nama_lengkap,
                      textScaleFactor: 1.35,
                      style: const TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Color.fromARGB(255, 14, 99, 223),
                      ),
                    ),
                  ),
                  Container(
                    child: Text(
                      "@" + username,
                      textScaleFactor: 1.1,
                      style: const TextStyle(
                        fontWeight: FontWeight.normal,
                        color: Color.fromARGB(255, 108, 117, 125),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ),
        ],
      )
    );
  }
}


class HalamanTokoKodeSisaPeriode extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return BorderedContainer(
        Container(
            padding: EdgeInsets.symmetric(vertical: 5),
            child: GridView.count(
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 3,
              mainAxisSpacing: 0,
              childAspectRatio: 2.8/1,
              shrinkWrap: true,
              children:
                  HalamanToko_KodeSisaPeriodeCell.juduls([
                    "kode saham", "sisa waktu", "periode dividen"
                  ]) +

                  HalamanToko_KodeSisaPeriodeCell.values([
                    "RAZE", "2 tahun", "12 bulan"
                  ]),
            )
        )
    );
  }
}

class HalamanToko_KodeSisaPeriodeCell{
  static List<Widget> juduls(List<String> judul) {
    return [
    for (var i=0; i < judul.length; i+=1)
      Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(vertical: 4),
        child: Text(judul[i],
          textScaleFactor: 0.72,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            color: Color.fromARGB(255, 155, 155, 155),
            fontFamily: 'Quicksand',
          ),
        ),
      ),
      ];
  }

  static List<Widget> values(List<String> nilai) {
    return [
      for (var i=0; i < nilai.length; i+=1)
      Container(
        alignment: Alignment.center,
        margin: EdgeInsets.symmetric(vertical: 4),
        child: Text(nilai[i],
          textScaleFactor: 1.15,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            color: Color.fromARGB(255, 255, 69, 0),
            fontFamily: 'Quicksand',
          ),
        ),
      ),
    ];
  }
}

class HalamanTokoKondisiSaham extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        HalamanTokoTabularData(
            child_aspect_ratio: 5/1,
            header_left: const Align(
              alignment: Alignment.centerLeft,
              child: ColouredHeaderText("Saham Tersisa"),
            ),

            header_right: const Align(
              alignment: Alignment.centerLeft,
              child:  ColouredHeaderText("Saham Terjual"),
            ),

            lines: const [
              "41.0 %", "59.0 %",
              "41 lembar", "59 lembar",
              "Rp41.000.000,00", "Rp59.000.000,00",
            ]),

        Container(
          margin: EdgeInsets.only(top: 14, bottom: 7),
          child: const ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(4)),
            child: LinearProgressIndicator(
              minHeight: 10,
              backgroundColor: Color.fromARGB(255, 212, 212, 212),
              color: Color.fromARGB(255, 13, 202, 240),
              value: 0.59,
            ),
          ),
        ),
      ],
    );
  }
}


class HalamanTokoTabularData extends StatelessWidget{
  final List<String> list;
  final TextStyle text_style;
  final double text_scale_factor;
  final double child_aspect_ratio;
  final TextAlign text_align;
  final Alignment cell_alignment;
  final Widget? header_left;
  final Widget? header_right;

  HalamanTokoTabularData({
    required List<String> lines,
    TextStyle text_style: const TextStyle(
      fontFamily: 'arial',
    ),
    TextAlign text_align: TextAlign.left,
    Alignment alignment: Alignment.centerLeft,
    double text_scale_factor: 0.85,
    double child_aspect_ratio: 4.7/1,

    Widget? header_left:null,
    Widget? header_right:null,

  }) : list = lines, text_style = text_style, text_scale_factor=text_scale_factor,
        text_align=text_align, cell_alignment=alignment,  header_left=header_left,
        header_right=header_right, child_aspect_ratio=child_aspect_ratio;

  @override
  Widget build(BuildContext context) {
    return GridView.count(
        physics: NeverScrollableScrollPhysics(),
        crossAxisCount: 2,
        mainAxisSpacing: 0,
        childAspectRatio: child_aspect_ratio,

        shrinkWrap: true,
        children:
            ((header_left==null)? <Widget>[]:<Widget>[header_left!]) +
            ((header_right==null)? <Widget>[]:<Widget>[header_right!]) +

            <Widget>[
                for (var i=0; i < list.length; i++)
                  Align(
                    alignment: cell_alignment,
                    child:
                    Text(list[i],
                      style: text_style,
                      textScaleFactor: text_scale_factor,
                      textAlign: text_align,
                    ),
                  )
            ],

    );
  }
}

class HalamanTokoAlamatDeskripsi extends StatelessWidget{
  final double headerTextScaleFactor = 1.2;
  final double left_margin = 15;
  final double right_margin = 5;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(7),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ColouredHeaderText("Alamat", textScaleFactor: headerTextScaleFactor,),
          Container(  // dummy container
            margin: EdgeInsets.all(4),
          ),
          Container(
              margin: EdgeInsets.only(left: left_margin, right: right_margin),
              child: const Text(
                "Jalan pepaya",
                textAlign: TextAlign.justify,
              )
          ),

          Container(  // dummy container
            margin: EdgeInsets.all(15),
          ),

          ColouredHeaderText("Deskripsi", textScaleFactor: headerTextScaleFactor,),
          Container(  // dummy container
            margin: EdgeInsets.all(4),
          ),
          Container(
              margin: EdgeInsets.only(left: left_margin, right: right_margin),
              child: const Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam vel leo nunc. Etiam vitae ligula vitae arcu maximus tincidunt vitae et velit. Mauris velit quam, venenatis quis viverra ultrices, viverra sit amet purus. Curabitur nec tempus velit. Integer vehicula elit vel augue fringilla, vitae dignissim dui viverra",
                textAlign: TextAlign.justify,
              )
          ),
        ],
      ),
    );
  }
}




class BorderedButtonIcon extends StatelessWidget{
  final Function()? on_pressed;
  final Text label;
  final Widget icon;

  BorderedButtonIcon({
    @required Function()? onPressed,
    Widget icon: const FaIcon(FontAwesomeIcons.book),
    Text label: const Text("Download Proposal", textScaleFactor: 1.2,)
  }) : on_pressed = onPressed, icon=icon, label=label;


  @override
  Widget build(BuildContext context) {
    return Container(
          child: ElevatedButton.icon(
            onPressed: on_pressed,
            label: label,
            style: const ButtonStyle(
              alignment: Alignment.centerLeft,
              /* TODO: bikin rounded corner kalau sempet */
            ),
            icon: Padding(
              child: icon,
              padding: get_padding(),
            ),
          ),
      margin: get_margin(),
    );
  }

  EdgeInsets get_padding(){
    return EdgeInsets.all(10.0);
  }

  EdgeInsets get_margin(){
    return EdgeInsets.symmetric(vertical: 8.0, horizontal: 25.0);
  }

}


class ColouredBorderedContainer extends BorderedContainer{
  final Color bg_color;
  ColouredBorderedContainer(Widget child, {
    Color bg_color=const Color.fromARGB(255, 14, 109, 254)
  }) : bg_color=bg_color, super(child);

  @override
  Color get_bg_color() {
    return bg_color;
  }
}


class BorderedContainer extends StatelessWidget{
  BorderedContainer(this.child);
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return
      Container(
          child: child,
          padding: get_padding(),
          margin: get_margin(),
          decoration: get_box_decoration(),
    );
  }

  EdgeInsets get_padding(){
    return EdgeInsets.all(10.0);
  }

  EdgeInsets get_margin(){
    // return EdgeInsets.zero;
    return EdgeInsets.symmetric(vertical: 8.0, horizontal: 25.0);
  }

  Color get_bg_color(){
    return Colors.white.withOpacity(0.75);
  }

  List<BoxShadow> get_box_shadows(){
    return [
      BoxShadow(
        color: Colors.grey.withOpacity(0.4),
        spreadRadius: 2,
        blurRadius: 3,
        offset: Offset(1, 1), // changes position of shadow
      )
    ];
  }


  Border get_border() {
    return Border.all(
      color: Colors.white,
    );
  }

  BorderRadius get_border_radius() {
    return BorderRadius.all(Radius.circular(10));
  }

  BoxDecoration get_box_decoration(){
    return BoxDecoration(
      color: get_bg_color(),
      border: get_border(),

      boxShadow: get_box_shadows(),
      borderRadius: get_border_radius(),

    );
  }
}