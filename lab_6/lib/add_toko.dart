import 'package:flutter/material.dart';
import "package:lab_6/halaman_toko.dart";


void main() {
  runApp(AddTokoMaterial());
}


const InputDecoration text_form_field_input_decoration = InputDecoration(
  labelText: 'Label text',
  floatingLabelBehavior: FloatingLabelBehavior.always,
  labelStyle: TextStyle(
    color: Color(0xFF0047D3),
  ),
  helperText: '',
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF28BCFF)),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(color: Color(0xFF28BCFF)),
  ),
  // filled: true,
  // fillColor: Color.fromARGB(140, 255, 255, 255),
);


class AddTokoMaterial extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      theme: ThemeData(
        textTheme: Theme.of(context).textTheme.apply(
            fontSizeFactor: 1.3,
            fontSizeDelta: 2.0,
            fontFamily: 'Tisan'
        ),
      ),

      home: SafeArea(child: Scaffold(
        body:SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 10),
          child: AddTokoBody("Bizzvest", "PT. Bizzvest Indonesia"),
        ),
        backgroundColor: (Colors.lightBlue[200])!,

        floatingActionButton: null,
      ),
      ),
    );
  }
}



class AddTokoBody extends StatelessWidget{
  final String nama_merek;
  final String nama_perusahaan;
  AddTokoBody(this.nama_merek, this.nama_perusahaan);

  DateTime picked_date = DateTime.now().add(Duration(days:1));

  @override
  Widget build(BuildContext context) {
    TextFormField berakhir_date;
    var berakhir_date_txt = TextEditingController();

    return
      Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [

          Container(
            margin: EdgeInsets.symmetric(vertical: 20,  horizontal: 0),
            child: BorderedContainer(
                Form(
                  child: Column(
                    children: [
                      SizedBox(height: 30,),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Nama merek",
                        ),
                      ),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Nama perusahaan",
                        ),
                      ),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Kode saham",
                        ),
                      ),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Alamat perusahaan",
                        ),
                      ),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Jumlah lembar saham",
                        ),
                      ),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Nilai lembar saham (rupiah)",
                        ),
                      ),
                      TextFormField(
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Dividen (bulan)",
                        ),
                      ),
                      berakhir_date = TextFormField(
                        controller: berakhir_date_txt,
                        readOnly: true,
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Batas waktu",
                        ),
                        onTap: (){
                          showDatePicker(
                              context: context,
                              initialDate: picked_date,
                              firstDate: DateTime.now().add(Duration(days:1)),
                              lastDate: DateTime.now().add(Duration(days:360*15)),
                          ).then((value){
                            if (value == null) return;
                            
                            String day = value.day.toString().padLeft(1, '0');
                            String month = value.month.toString().padLeft(1, '0');
                            String year = value.year.toString().padLeft(1, '0');
                            picked_date = value;
                            berakhir_date_txt.text = "$month/$day/$year";
                          });
                        },
                      ),

                      /*InputDatePickerFormField(
                        firstDate: DateTime.now(),
                        lastDate: DateTime.now().add(const Duration(days:360*15)),
                      ),*/
                      TextFormField(
                        textInputAction: TextInputAction.newline,
                        keyboardType: TextInputType.multiline,
                        enabled: true,
                        textAlign: TextAlign.justify,
                        minLines: 4,
                        maxLines: 20,
                        decoration: text_form_field_input_decoration.copyWith(
                          labelText: "Deskripsi",
                        ),
                      ),
                    ],
                  ),
                )
            ),
          ),
        ],
      );
  }
}


