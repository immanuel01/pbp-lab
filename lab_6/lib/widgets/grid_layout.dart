/*
import 'dart:math';



// ignore_for_file: curly_braces_in_flow_control_structures

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GridLayout extends StatefulWidget{
  const GridLayout({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return GridLayoutState();
  }
}


class GridLayoutState extends State<GridLayout>{
  late List<RowDefinition> row_definitions;
  late List<ColumnDefinition> col_definitions;
  late List<Widget> children;

  late List<int> row_definition_sorted_by_priority;
  late List<int> col_definition_sorted_by_priority;

  GridLayoutState({this.row_definitions=const [], this.col_definitions=const [],
  this.children=const []}){
    update_row();
    update_column();
  }
  update(){
    update_row();
    update_column();
  }

  update_row(){
    if (row_definitions.isEmpty)
      row_definitions.add(RowDefinition(GridLayoutUnit.STAR, 1));
    row_definition_sorted_by_priority = List.from(row_definitions);
    row_definition_sorted_by_priority.sort(
        (int a, int b) => row_definitions[a].priority - row_definitions[b].priority
    );
  }

  update_column(){
    if (col_definitions.isEmpty)
      col_definitions.add(ColumnDefinition());
    col_definition_sorted_by_priority = List.from(col_definitions);
    col_definition_sorted_by_priority.sort(
        (int a, int b) => col_definitions[a].priority - col_definitions[b].priority
    );
  }


  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return CustomMultiChildLayout(
            delegate: GridLayoutDelegate(
              col_definitions: col_definitions,
              row_definitions: row_definitions,
              col_definition_sorted_by_priority: col_definition_sorted_by_priority,
              row_definition_sorted_by_priority: row_definition_sorted_by_priority,
              max_height: constraints.maxHeight,
              max_width: constraints.maxWidth,
              number_of_child: children.length,
            ),

            children: [
              for (int i=0; i<children.length; i++)
                LayoutId(
                  id: i,
                  child: children[i],
                )
            ],
          );
        },
    );
  }
}






class GridLayoutDelegate extends MultiChildLayoutDelegate{
  bool force_relayout = false;
  final double max_height;
  final double max_width;
  final List<RowDefinition> row_definitions;
  final List<ColumnDefinition> col_definitions;
  final List<int> row_definition_sorted_by_priority;
  final List<int> col_definition_sorted_by_priority;
  final int number_of_child;


  int get row_num => row_definitions.length;
  int get col_num => col_definitions.length;


  GridLayoutDelegate({
    required this.max_height,
    required this.max_width,
    required this.row_definitions,
    required this.col_definitions,
    required this.col_definition_sorted_by_priority,
    required this.row_definition_sorted_by_priority,
    required this.number_of_child});


  @override
  void performLayout(Size size) {
    List<Size?> sizes = [
      for (int i=0; i < row_definitions.length; i++)
        null
    ];

    List<double> row_constrains = [
      for (int i=0; i < row_definitions.length; i++)
        -double.infinity
    ];

    List<double> col_constrains = [
      for (int i=0; i < col_definitions.length; i++)
        -double.infinity
    ];


    double height_star_total = 0;
    double width_star_total = 0;

    row_definitions.forEach((element) {
      if (element.unit == GridLayoutUnit.STAR)
        height_star_total += element.value;
    });

    col_definitions.forEach((element) {
      if (element.unit == GridLayoutUnit.STAR)
        width_star_total += element.value;
    });

    for (int r = 0; r < row_definitions.length; r++){
      if (row_definitions[r].unit == GridLayoutUnit.STAR)
        continue;

      if (row_definitions[r].unit == GridLayoutUnit.PX)
        row_constrains[r] = row_definitions[r].value;

      for (int c = 0; c < col_definitions.length; c++){
        int this_id = r * col_num + c;
        if (sizes[this_id] != null) continue;
        if (col_definitions[r].unit == GridLayoutUnit.STAR)
          continue;
        if (col_definitions[c].unit == GridLayoutUnit.PX)
          col_constrains[c] = col_definitions[c].value;

        if (row_definitions[r].unit == GridLayoutUnit.PX
            && col_definitions[c].unit == GridLayoutUnit.PX){

            sizes[this_id] = layoutChild(this_id, BoxConstraints(
              minHeight: 0, minWidth: 0,
              maxHeight: row_definitions[r].value, maxWidth: col_definitions[c].value,
            ));
        }else if (row_definitions[r].unit == GridLayoutUnit.AUTO
            && col_definitions[c].unit == GridLayoutUnit.PX){

          sizes[this_id] = layoutChild(this_id, BoxConstraints(
            minHeight: 0, minWidth: 0,
            maxHeight: this.max_height, maxWidth: col_definitions[c].value,
          ));
          row_constrains[r] = max(row_constrains[r], sizes[this_id].height);
        }else if (row_definitions[r].unit == GridLayoutUnit.PX
            && col_definitions[c].unit == GridLayoutUnit.AUTO){

          sizes[this_id] = layoutChild(this_id, BoxConstraints(
            minHeight: 0, minWidth: 0,
            maxHeight: row_definitions[r].value, maxWidth: this.max_width,
          ));
          col_constrains[c] = max(col_constrains[c], sizes[this_id]!.width);
        }else if (row_definitions[r].unit == GridLayoutUnit.AUTO
            && col_definitions[c].unit == GridLayoutUnit.AUTO){

          sizes[this_id] = layoutChild(this_id, BoxConstraints(
            minHeight: 0, minWidth: 0,
            maxHeight: this.max_height, maxWidth: this.max_width,
          ));
          col_constrains[c] = max(col_constrains[c], sizes[this_id]!.width);
          row_constrains[r] = max(row_constrains[r], sizes[this_id]!.height);
        }
      }
    }

    double used_width = 0;
    double used_height = 0;

    for (int i=0; i < row_constrains.length; i++){
      if (row_constrains[i] == -double.infinity)
        continue;
      used_height += row_constrains[i];
    }

    for (int i=0; i < col_constrains.length; i++){
      if (col_constrains[i] == -double.infinity)
        continue;
      used_width += col_constrains[i];
    }

    double unused_width = max(0, max_width - used_width);
    double unused_height = max(0, max_height - used_height);

    // TODO ngelanjutin handle para star

  }

  @override
  bool shouldRelayout(GridLayoutDelegate oldDelegate) {
    bool ret = max_height != oldDelegate.max_height
        || max_width != oldDelegate.max_width
        || force_relayout;
    force_relayout = false;
    return ret;
  }
}




abstract class RowColDefinition{
  late int priority;
  late double value;
  late GridLayoutUnit unit;
  // late int pos;

  RowColDefinition([GridLayoutUnit? unit, double? value, int? priority, int pos=-1]){
    this.unit = unit ?? GridLayoutUnit.PX;
    this.value = value ?? (
        (this.unit == GridLayoutUnit.STAR)? 1:0
    );
    this.priority = priority ?? this.unit.default_priority;
    // this.pos = (pos >= 0)? pos:-1;
  }

}

class RowDefinition extends RowColDefinition{
  RowDefinition([GridLayoutUnit? unit, double? value, int? priority]) : super(unit, value, priority);
}

class ColumnDefinition extends RowColDefinition{
  ColumnDefinition([GridLayoutUnit? unit, double? value, int? priority]) : super(unit, value, priority);
}

enum GridLayoutUnit{
  AUTO, PX, STAR
}

extension GridLayoutUnitExtension on GridLayoutUnit{
  int get default_priority{
    switch (this){
      case GridLayoutUnit.PX:
        return 30;
      case GridLayoutUnit.AUTO:
        return 20;
      case GridLayoutUnit.STAR:
        return 10;
    }
  }
}



*/
