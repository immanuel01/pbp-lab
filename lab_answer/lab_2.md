### Apakah perbedaan antara JSON dan XML?

JSON

- Merupakan singkatan dari JavaScript Object Notation
- Pada dasarnya, mirip seperti syntax pada JavaScript
- Relatif lebih ringkas dan singkat
- Tidak dapat mengandung komentar
- Tidak mendukung metadata (sejenis informasi tambahan pada suatu objek)
- Tidak dapat digunakan dalam membuat desain dari suatu aplikasi
- Relatif lebih cepat diproses oleh komputer


XML
- Merupakan singkatan dari eXtensible Markup Language
- Pada dasarnya, mirip seperti syntax pada HTML; menggunakan tag pembuka dan tag penutup
- Relatif membutuhkan lebih banyak karakter untuk menyimpan suatu data
- Bisa mengandung komentar
- Mendukung metadata (sejenis informasi tambahan pada suatu objek), yakni berupa atribut
- Bisa dimanfaatkan dalam membuat desain dari suatu aplikasi (misalnya pada WPF dan Android Studio)
- Relatif lebih lambat saat diproses oleh komputer



### Apakah perbedaan antara HTML dan XML?

HTML
- Singkatan dari HyperText Markup Language
- Lebih sering digunakan dalam mendeskripsikan tampilan dari suatu halaman website maupun aplikasi (misalnya Electron)
- Umumnya kita hanya dapat menggunakan nama-nama tag yang sudah didefinisikan dan disepakati oleh banyak pihak 
- Pada umumnya, whitespace tidak begitu berpengaruh (kecuali untuk `<pre>` tag)
- Bersifat case insensitive (tidak terpengaruh oleh kapitalisasi) 
 

XML
- eXtensible Markup Language
- Lebih banyak digunakan sebagai format penyimpanan suatu data yang readable maupun sebagai desain dari tampilan suatu aplikasi (misalnya pada WPF maupun Android Studio)
- Kita dapat menggunakan nama tag apapun secara bebas sesuai dengan keinginan dan kebutuhan kita
- Whitespace sangat berpengaruh
- Bersifat case sensitive (dipengaruhi oleh kapitalisasi)