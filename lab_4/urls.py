from django.urls import path
import lab_4.views as views

urlpatterns = [
    path('', views.index, name='index_lab_4'),
    path('add', views.add_note, name='add_note_lab_4'),
    path('note-list', views.note_list, name='note_list'),
]

# /lab-2/xml