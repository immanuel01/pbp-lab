from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from lab_2.models import Note
from .forms import NoteForm

def index(req):
    data = {
        'notes' : Note.objects.all()
    }
    return render(req, "lab4_index.html", data)


def note_list(req):
    data = {
        'notes' : Note.objects.all()
    }
    return render(req, "lab4_note_list.html", data)


def add_note(req):
    formulir = NoteForm(req.POST or None)

    if (formulir.is_valid() and req.method == "POST"):
        formulir.save()
        return HttpResponseRedirect(reverse("index_lab_4"))

    context = {
        'formulir': formulir
    }
    return render(req, "lab4_form.html", context)