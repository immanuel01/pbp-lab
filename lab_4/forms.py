import django.forms
from django.forms import ModelForm
from lab_2.models import Note


class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields = ['dari', 'to', 'title', 'message']

        widgets = {
            'message': django.forms.Textarea()  # supaya field message berubah menjadi textarea
        }

        # ketika pengguna iseng menghapus attribute "required" dari form dan kemudian memaksa mengisi form yang invalid
        error_messages = {
            'dari' : {
                'invalid': "nama pengirim invalid",
                'required': "mohon mengisi nama pengirim"
            },
            'to' : {
                'invalid': "nama tujuan invalid",
                'required': "mohon mengisi nama tujuan"
            },
            'title' : {
                'invalid': "judul invalid",
                'required': "mohon mengisi judul"
            },
            'message' : {
                'invalid': "isi pesan invalid",
                'required': "mohon mengisi isi pesan"
            },
        }

