from django.db import models
from django.forms import ModelForm, CharField, DateField, TextInput, DateInput, NumberInput
from lab_1.models import Friend


class FriendForm(ModelForm):
    class Meta:
        model = Friend
        fields = ['name', 'npm', 'DOB']

        # ketika pengguna iseng menghapus attribute "required" dari form dan kemudian memaksa mengisi form yang invalid
        error_messages = {
            'name' : {  'invalid': "nama invalid",             'required': "mohon mengisi nama"},
            'npm' : {   'invalid': "npm invalid",              'required': "mohon mengisi npm"},
            'DOB' : {   'invalid': "tanggal lahir invalid",    'required': "mohon mengisi tanggal lahir"},
        }

        name = CharField(label="", required=True, max_length=30, widget=TextInput(attrs={
            'type': 'text',
            'placeholder': 'nama Anda'
        }))

        npm = CharField(label="", required=True, min_length=10, max_length=10, widget=NumberInput(attrs={
            'placeholder': 'NPM Anda'
        }))

        DOB = DateField(label="", required=True, widget=DateInput())



