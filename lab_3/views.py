from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import render
from datetime import datetime, date

from django.urls import reverse

from lab_1.models import Friend
from lab_3.forms import FriendForm


curr_year = int(datetime.now().strftime("%Y"))

@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all().values()
    response = {'friends': friends}
    return render(request, 'friend_list_lab1.html', response)


def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

@login_required(login_url="/admin/login/")
def add_friend(request):
    friend_form = FriendForm(request.POST or None)

    if (friend_form.is_valid() and request.method == 'POST'):
        friend_form.save()
        return HttpResponseRedirect(reverse('lab_3_index'))

    context = {
        'form' : friend_form.as_p()
    }

    return render(request, "lab3_form.html", context)
