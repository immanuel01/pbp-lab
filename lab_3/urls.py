from django.urls import path
import lab_3.views as views

urlpatterns = [
    path('', views.index, name='lab_3_index'),  # /lab-3/
    path('add', views.add_friend, name='add_friend'),
]
