from django.db import models

# TO DO Create Friend model that contains name, npm, and DOB (date of birth) here


class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=30)
    DOB = models.DateField(max_length=30)
    # TO DO Implement missing attributes in Friend model
