from django.urls import path
from .views import index, friend_list

# M -> Model T -> Template V -> Views

urlpatterns = [
    # TO DO Add friends path using friend_list Views
    path('', index, name='index'),
    path('friends', friend_list),
]
